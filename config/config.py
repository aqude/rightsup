
class Config:
    def __init__(self, config_data):
        self.config = config_data

    def get_proxy_login(self):
        return self.config["proxy"]["login"]

    def get_proxy_password(self):
        return self.config["proxy"]["password"]

    def get_proxy_address(self):
        return self.config["proxy"]["address"]
