import asyncio
import os

import bs4
import requests
import yaml

from config.config import Config


async def parse():
    text = []
    with open("./config/config.yaml", "r") as yaml_file:
        config = yaml.safe_load(yaml_file)
    cfg = Config(config)
    proxy_login = cfg.get_proxy_login()
    proxy_password = cfg.get_proxy_password()
    proxy_address = cfg.get_proxy_address()
    proxy = {
        "https": f"http://{proxy_login}:{proxy_password}@{proxy_address}",
        "http": f"http://{proxy_login}:{proxy_password}@{proxy_address}"
    }

    i = 1
    i_article = 0
    while True:
        url = f"https://sputnikipogrom.com/tag/premiumrussian/page/{i}/"

        try:
            res = requests.get(url, proxies=proxy, timeout=5)
            if res.status_code == 200:
                soup = bs4.BeautifulSoup(res.content, 'lxml')
                nlg_cols = soup.find_all('div', class_='nlg-col-wrap nlg-col-3')
                for col in nlg_cols:
                    articles = col.find_all('article', class_='nlg-entry')
                    for article in articles:
                        i_article += 1
                        nlg_date = article.find('li', class_='nlg-date')
                        a_tag = nlg_date.find('a')
                        if a_tag and 'href' in a_tag.attrs:
                            link = a_tag['href']
                            print(link)
                            response = requests.get(link, proxies=proxy, timeout=5)
                            if response.status_code == 200:
                                article_soup = bs4.BeautifulSoup(response.content, 'lxml')
                                paragraphs = article_soup.find_all('p')
                                for paragraph in paragraphs:
                                    paragraph_text = paragraph.get_text()
                                    text.append(paragraph_text)
                                    print(paragraph_text)
                            else:
                                print("Request failed with status code (num 2):", link.status_code)
            if res.status_code == 404:
                print("No more pages to parse (404 Not Found)")
                break
            else:
                print("Request failed with status code:", res.status_code)

        except requests.RequestException as e:
            print("Error:", e)

        i += 1
    print("Колличество статей: " + str(i_article))
    return text


def data(text: list[str], file_path: str):
    try:
        if not os.path.exists(file_path):
            with open(file_path, 'w', encoding='utf-8') as file:
                for t in text:
                    file.write(t + '\n')
        else:
            with open(file_path, 'a', encoding='utf-8') as file:
                for t in text:
                    file.write(t + '\n')
        return True
    except Exception as e:
        print("Error:", e)
        return False


text = asyncio.run(parse())
if data(text, "./dataset/dataset.txt"):
    print("Данные успешно спаршены, файл создан.")
